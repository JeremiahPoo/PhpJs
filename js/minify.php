<?php 
/**
 * script for minify js 
 * 
 */

$c = file_get_contents("FetchPhpJs.js.dist");
$c = preg_replace("#(\t+|\s+|\r\n)((/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(//.*))#","",$c); 
$c = preg_replace("#\n#", "", $c); 

$f = fopen("FetchPhpJs.js.dist.min", "w"); 
fwrite($f,$c); 
fclose($f);
