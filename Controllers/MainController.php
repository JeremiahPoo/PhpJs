<?php 

class MainController extends Controller {

    public function main() {
       
        $text = $this->getArgument("arg");
        $text2 = $this->getArgument("arg2");
        $basePath = $this->app->getBasePath();
   
        return $this->render->view("Components/main.phtml", array(
            "test" => $text,
            "otherVar" => $text2,
            "basePath" => $basePath
        ));
    }

    public function secondPage() {
        return $this->render->view("Components/secondPage.phtml");
    }

    public function tablePage() {

        return $this->render->fetchView("Components/tab.phtml");
    }

    public function anotherPage() {

        return $this->render->fetchView("Components/anotherPage.phtml");
    }

}