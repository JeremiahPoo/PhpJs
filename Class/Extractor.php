<?php 

require_once __DIR__."/../vendor/autoload.php";

/**
 * 
 * require Symphony Crawler for work
 * 
 */
use Symfony\Component\DomCrawler\Crawler;

class Extractor {


    protected $config = [];
    protected $html;
    protected $sleep = 5; 
    protected $proxyUrl = "http://sweb.semantiweb.fr/scripts/get_proxies_list.php";
    protected $useProxy = false;
    protected $proxiesList;
    protected $proxy_id = 0;

    public function __construct($config) {
        $this->setConfig($config);

        if($this->useProxy) {
            $this->getProxy();
        }
    }

    public function extractContent($url, $cssSelector) {
        $html = $this->curlGetContent($url);
        $crawler = new Crawler($html);
        $list = [];
        $nodes = $crawler->filter($cssSelector);
        
        foreach($nodes as $node) {
         
            $list[] = $node->nodeValue;
        }

        return $list;
    }

    public function extractContentArray($url, $cssSelectors) {
        $html = $this->curlGetContent($url);
        $data = [];
        foreach($cssSelectors as $cssSelector) {
            $crawler = new Crawler($html);
            $list = [];
            $nodes = $crawler->filter($cssSelector);
            foreach($nodes as $node) {
     
                $list[] = $node->nodeValue;
            }
    
            $data[$cssSelector] = $list;
        }
        return $data;
    }

    public function extractContentPrec($url, $cssSelector, $array) {
        $html = $this->curlGetContent($url);
        $crawler = new Crawler($html);
        $list = [];
        $nodes = $crawler->filter($cssSelector)->extract($array);
     
        foreach($nodes as $node) {
 
            $list[] = $node;
        }

        return $list;
    }

    public function curlGetContent($url) {

       $try = 0;
        while($try < 10) {
            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, $url);  
    
            if($this->useProxy) {
                if($this->proxy_id == count($this->proxiesList) - 1) {
                    $this->proxy_id = 0; 
                }
                $proxy = $this->proxiesList[$this->proxy_id];
                curl_setopt($c, CURLOPT_PROXY, $proxy);
                curl_setopt($c, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
                $this->proxy_id++;
            }
    
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
         
            $contents = curl_exec($c);
            $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
            curl_close($c);
       
            if ($contents) {
                return $contents;
            } else {
                $try++;
            }
        }

    
        return $err;
        
    }

    protected function getProxy() {
        $fileManager = new FilesManager;
        $all_proxies = $fileManager->extractJson($this->proxyUrl); 

        $first_ind = ($nb_proxies*$lot)-$nb_proxies;
        $last_ind = ($first_ind+$nb_proxies)-1;

        $proxies_list = array();

        for($i=$first_ind;$i <= $last_ind;$i++){
            array_push($proxies_list,$all_proxies[$i]);
        }

        
        $this->proxiesList = $proxies_list;
    }

    protected function sleep() {
        sleep($this->sleep);
    }

     /**
     * Get the value of config
     */ 
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set the value of config
     *
     * @return  self
     */ 
    public function setConfig($config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get the value of sleep
     */ 
    public function getSleep()
    {
        return $this->sleep;
    }

    /**
     * Set the value of sleep
     *
     * @return  self
     */ 
    public function setSleep($sleep)
    {
        $this->sleep = $sleep;

        return $this;
    }

    /**
     * Get the value of useProxy
     */ 
    public function getUseProxy()
    {
        return $this->useProxy;
    }

    /**
     * Set the value of useProxy
     *
     * @return  self
     */ 
    public function setUseProxy($useProxy)
    {
        $this->useProxy = $useProxy;

        return $this;
    }

    /**
     * Get the value of proxyUrl
     */ 
    public function getProxyUrl()
    {
        return $this->proxyUrl;
    }

    /**
     * Set the value of proxyUrl
     *
     * @return  self
     */ 
    public function setProxyUrl($proxyUrl)
    {
        $this->proxyUrl = $proxyUrl;

        return $this;
    }
}