<?php 

/*
Solr Library for easily call solr 

Developped by Quentin Gary 

V : 0.1

WIP 
*/
class Solr {


    const _BRAND = "marque";
    const _STARS = "stars";
    const _SITELABEL = "site_label";
    const _COUNTRY = "country";
    const _YEARS = "year";
    const _MONTH = "month";
    const _CATEGORY = "category";
    const _PRODUCTS = "prod_name_n1";

    const FACETSORT_LEX = "lex";
    const FACETSORT_COUNT = "count";
    const FACETSORT_INDEX = "index";

    const FACETMETHOD_ENUM = "enum";
    const FACETMETHOD_FC = "fc";

    const FACETFIELD_KW_CONTENT = "kw_content";
    const FACETFIELD_CATEGORY = "category";
    const FACETFIELD_SUBCATEGORY = "subcategory";
    const FACETFIELD_PRODUCTS = "prod_name_n1";
    const FACETFIELD_SITELABEL = "site_label";
    const FACETFIELD_STARS = "stars";

    const COUNTRY_US = "usa";
    const COUNTRY_FR = "french";

    protected $q = "*:*";
    protected $start = "0";
    protected $rows = "1";
    protected $wt = "json";
    protected $indent = "on";
    protected $facet = "true";
    protected $facetMethod = "enum";
    protected $facetLimit = "-1";
    protected $facetThreads = "8";
    protected $facetMinCount = "0";
    protected $facetSort = "lex";

    protected $facetField = [];
    

    protected $brands = [];
    protected $stars = [];
    protected $country = [];
    protected $month = [];
    protected $siteLabel = [];
    protected $years = [];
    protected $category = [];
    protected $products = [];

    protected $solrUrl;
    protected $solrCorpus;
    

    public function __construct($solrUrl, $solrCorpus) { // http://index-fr.semantiweb.fr:8080/solr/ , ratings_reviews_final

        if(!is_string($solrUrl) || !is_string($solrCorpus)) {
            throw new Exception("The arguments must be a string");
        }

        $this->solrUrl = $solrUrl;
        $this->solrCorpus = $solrCorpus;
    }   

    protected function generateSolrString() {
        $solrString = $this->solrUrl.$this->solrCorpus;
        $solrString .= "/select?q=".$this->q;
        $solrString .= "&start=".$this->start;
        $solrString .= "&rows=".$this->rows;
        $solrString .= "&wt=".$this->wt;
        $solrString .= "&indent=".$this->indent;
        $solrString .= "&facet=".$this->facet;
        $solrString .= "&facet.method=".$this->facetMethod;
        $solrString .= "&facet.limit=".$this->facetLimit;
        $solrString .= "&facet.threads=".$this->facetThreads;
        $solrString .= "&facet.mincount=".$this->facetMinCount;
        $solrString .= "&facet.sort=".$this->facetSort;

        foreach($this->facetField as $field) {
            $solrString .= "&facet.field=".$field;
        }

        $this->foreachFq($solrString , $this->brands, self::_BRAND);
        $this->foreachFq($solrString , $this->stars, self::_STARS);
        $this->foreachFq($solrString , $this->country, self::_COUNTRY);
        $this->foreachFq($solrString , $this->month, self::_MONTH);
        $this->foreachFq($solrString , $this->siteLabel, self::_SITELABEL);
        $this->foreachFq($solrString , $this->years, self::_YEARS);
        $this->foreachFq($solrString , $this->category, self::_CATEGORY);
        $this->foreachFq($solrString , $this->products, self::_PRODUCTS);

        $solrString = str_replace(' ', '+', $solrString);
        return $solrString;
    }

    /** 
     * 
     * @return string return the path of the solr call 
     * 
     */
    public function getSolrString() {

        $string = $this->generateSolrString();

        return $string;
    }
    /**
     * 
     * @return array return the result of the search on a php array format 
     * 
     */

    public function getResult() {

        $try = 0;

        while(1) {
            $data = $this->curlGetContent($this->generateSolrString());

            if(is_array(json_decode($data, true)) || $try > 10) {
                break; 
            }

            $try++;
        }
        
        return json_decode($data, true);
    }

    /**
     * 
     * @return array return the result of the search on a json array format 
     * 
     */

    public function getResultJson() {

        $try = 0;

        while(1) {
            $data = $this->curlGetContent($this->generateSolrString());

            if(is_array(json_decode($data, true)) || $try > 10) {
                break; 
            }

            $try++;
        }
        
        return $data;
    }

    protected function foreachFq(&$solrString, $fq , $field) {
        if(count($fq) == 0 ) {
            return;
        } elseif(count($fq) == 1 ) {
            $solrString .= "&fq=".$field.":".$fq[0];
        } else {
            $solrString .= "&fq=".$field.":(";
            $index = 0;
            foreach($fq as $q) {
                if($index == 0 ) {
                    $solrString .= $q;
                } else {
                    $solrString .= " OR ".$q;
                }
                
                
                $index++;
            }
            $solrString .= ")";
        }
    } 

    protected function curlGetContent($url) {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
     
        $contents = curl_exec($c);
        $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
        curl_close($c);
   
        if ($contents) {
            return $contents;
        } 
        else {
            return $err;
        } 
    }




    /**
     * Get the value of country
     */ 
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of country
     * @param array $country must be a array of the different wanted countries 
     * @return  self
     */ 
    public function setCountry($country)
    {   
        if(!is_array($country)) {
            $type = gettype($country);
            throw new Exception("The argument must be a array.".$type." returned");
        }
        $this->country = $country;

        return $this;
    }

    /**
     * Get the value of facetField
     */ 
    public function getFacetField()
    {   
        return $this->facetField;
    }

    /**
     * Set the value of facetField
     *
     * @return  self
     */ 
    public function setFacetField($facetField)
    {   
        if(!is_array($facetField)) {
            $type = gettype($facetField);
            throw new Exception("The argument must be a array.".$type." returned");
        }
        $this->facetField = $facetField;

        return $this;
    }

    /**
     * Get the value of stars
     */ 
    public function getStars()
    {   
        return $this->stars;
    }

    /**
     * Set the value of stars
     * @param array $stars must contain the stars for the search 
     * @return  self
     */ 
    public function setStars($stars)
    {   
        if(!is_array($stars)) {
            $type = gettype($stars);
            throw new Exception("The argument must be a array.".$type." returned");
        }
        $this->stars = $stars;

        return $this;
    }

    /**
     * Get the value of brands
     */ 
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * Set the value of brands
     * @param array $brands must contain the brand list for the search 
     * @return  self
     */ 
    public function setBrands($brands)
    {   
        if(!is_array($brands)) {
            $type = gettype($brands);
            throw new Exception("The argument must be a array.".$type." returned");
        }
        $this->brands = $brands;

        return $this;
    }

    /**
     * Get the value of start
     */ 
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set the value of start
     *
     * @return  self
     */ 
    public function setStart($start)
    {   
        if(is_numeric($start)) {
            $start = strval($start);
        }
        $this->start = $start;

        return $this;
    }

    /**
     * Get the value of rows
     */ 
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * Set the value of rows
     *
     * @return  self
     */ 
    public function setRows($rows)
    {   
        if(is_numeric($rows)) {
            $rows = strval($rows);
        }
        $this->rows = $rows;

        return $this;
    }

    /**
     * Get the value of wt
     */ 
    public function getWt()
    {
        return $this->wt;
    }

    /**
     * Set the value of wt
     *
     * @return  self
     */ 
    public function setWt($wt)
    {
        $this->wt = $wt;

        return $this;
    }

    /**
     * Get the value of indent
     */ 
    public function getIndent()
    {
        return $this->indent;
    }

    /**
     * Set the value of indent
     *
     * @return  self
     */ 
    public function setIndent($indent)
    {
        $this->indent = $indent;

        return $this;
    }

    /**
     * Get the value of facet
     */ 
    public function getFacet()
    {
        return $this->facet;
    }

    /**
     * Set the value of facet
     * @param bool $facet 
     * @return  self
     */ 
    public function setFacet($facet)
    {   
        if(!is_bool($facet)) {
            $type = gettype($facet);
            throw new Exception("The argument passed to the function 'setFacet' must be a bool.".$type." returned");
        }

        if($facet == true) {
            $facet = "true";
        } elseif($facet == false) {
            $facet = "false";
        }
        $this->facet = $facet;

        return $this;
    }

    /**
     * Get the value of facetMethod
     */ 
    public function getFacetMethod()
    {
        return $this->facetMethod;
    }

    /**
     * Set the value of facetMethod
     *
     * @return  self
     */ 
    public function setFacetMethod($facetMethod)
    {
        $this->facetMethod = $facetMethod;

        return $this;
    }

    /**
     * Get the value of facetLimit
     */ 
    public function getFacetLimit()
    {
        return $this->facetLimit;
    }

    /**
     * Set the value of facetLimit
     *
     * @return  self
     */ 
    public function setFacetLimit($facetLimit)
    {   
        if(is_numeric($facetLimit)) {
            $facetLimit = strval($facetLimit);
        }
        $this->facetLimit = $facetLimit;

        return $this;
    }

    /**
     * Get the value of facetThreads
     */ 
    public function getFacetThreads()
    {
        return $this->facetThreads;
    }

    /**
     * Set the value of facetThreads
     *
     * @return  self
     */ 
    public function setFacetThreads($facetThreads)
    {   
        if(is_numeric($facetThreads)) {
            $facetThreads = strval($facetThreads);
        }
        $this->facetThreads = $facetThreads;

        return $this;
    }


    /**
     * Get the value of facetMinCount
     */ 
    public function getFacetMinCount()
    {
        return $this->facetMinCount;
    }

    /**
     * Set the value of facetMinCount
     *
     * @return  self
     */ 
    public function setFacetMinCount($facetMinCount)
    {   
        if(is_numeric($facetMinCount)) {
            $facetMinCount = strval($facetMinCount);
        }
        $this->facetMinCount = $facetMinCount;

        return $this;
    }

    /**
     * Get the value of facetSort
     */ 
    public function getFacetSort()
    {
        return $this->facetSort;
    }

    /**
     * Set the value of facetSort
     *
     * @return  self
     */ 
    public function setFacetSort($facetSort)
    {
        $this->facetSort = $facetSort;

        return $this;
    }

    /**
     * Get the value of siteLabel
     */ 
    public function getSiteLabel()
    {
        return $this->siteLabel;
    }

    /**
     * Set the value of siteLabel
     * @param array $siteLabel must contain the site label for the search 
     * @return  self
     */ 
    public function setSiteLabel($siteLabel)
    {   
        if(!is_array($siteLabel)) {
            $type = gettype($siteLabel);
            throw new Exception("The argument must be a array.".$type." returned");
        }
        $this->siteLabel = $siteLabel;

        return $this;
    }

    /**
     * Get the value of q
     */ 
    public function getQ()
    {
        return $this->q;
    }

    /**
     * Set the value of q
     *
     * @return  self
     */ 
    public function setQ($q)
    {
        $this->q = $q;

        return $this;
    }

    /**
     * Get the value of years
     */ 
    public function getYears()
    {
        return $this->years;
    }

    /**
     * Set the value of years
     * @param array
     * @return  self
     */ 
    public function setYears($years)
    {
        if(!is_array($years)) {
            $type = gettype($years);
            throw new Exception("The argument must be a array.".$type." returned");
        }
        $this->years = $years;

        return $this;
    }

    /**
     * Get the value of month
     */ 
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set the value of month
     * @param array
     * @return  self
     */ 
    public function setMonth($month)
    {   
        if(!is_array($month)) {
            $type = gettype($month);
            throw new Exception("The argument must be a array.".$type." returned");
        }
        $this->month = $month;

        return $this;
    }

    /**
     * Get the value of category
     */ 
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set the value of category
     * @param array
     * @return  self
     */ 
    public function setCategory($category)
    {   if(!is_array($category)) {
            $type = gettype($category);
            throw new Exception("The argument must be a array. ".$type." returned");
        }
        $this->category = $category;

        return $this;
    }

    /**
     * Get the value of products
     */ 
    public function getProducts()
    {   
        return $this->products;
    }

    /**
     * Set the value of products
     * @param array
     * @return  self
     */ 
    public function setProducts($products)
    {   
        if(!is_array($products)) {
            $type = gettype($products);
            throw new Exception("The argument must be a array. ".$type." returned");
        }
        $this->products = $products;

        return $this;
    }
}