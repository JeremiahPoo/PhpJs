<?php 

require_once "Solr.php";
require_once "ConsoleSpeaker.php";


/**
 *   V : 0.1
 * 
 *  Solr response manager 
 * 
 *  Developped by Q.Gary 
 * 
 *  WIP
 * 
 *
 */ 


class SolrResponseManager {

    const DEBUG = 1;

    protected $response = [];
    protected $consoleSpeaker;
    protected $mode = 0;

   
    /**
     * @param array $response the solr php response array from solr library "getResult()" function 
     */
    public function __construct($response) {
        if(!is_array($response)) {
            $type = gettype($response);
            throw new Exception("The argument passed in the constructor must be a array.".$type." returned");
        } else {
            $this->response = $response;
            $this->consoleSpeaker = new ConsoleSpeaker;
        }
    }



    /**
     * @return string numFound 
     */
    public function getNumFound() {
        return $this->response["response"]["numFound"];
    }



    /**
     * @return array top50 kw array
     */
    public function getKwContent() {
        $array = $this->response["facet_counts"]["facet_fields"]["kw_content"];
        $topArray = [];
        for($i = 0 ; $i < count($array) ; $i+=2) {
            $topArray[$array[$i]] = $array[$i+1];
        }

        return $topArray;
    }

    protected function makeArrayWithSolrFacetField($array) {

        if(!is_array($array)) {
            $type = gettype($array);
            throw new Exception("The argument passed in the function 'makeArrayWithSolrFacetField()' must be a array.".$type." returned");
        }
        $topArray = [];
        for($i = 0 ; $i < count($array) ; $i+=2) {
            $topArray[$array[$i]] = $array[$i+1];
        }

        return $topArray;
    }



    public function getFacetFieldStars(){
        if(!array_key_exists("stars", $this->response["facet_counts"]["facet_fields"])) {

            throw new Exception("The field stars doesn't exist ");
        }
        $array = $this->response["facet_counts"]["facet_fields"]["stars"];

        return $array;
    }



    /**
     * 
     * @param string the name of the facet 
     * @return array the result array 
     * 
     */
    public function getFacetField($facetName, $makeArray = true , $clean = true) {

        if(!array_key_exists($facetName, $this->response["facet_counts"]["facet_fields"])) {

            throw new Exception("The field ".$facetName." doesn't exist ");
        }

        $array = $this->response["facet_counts"]["facet_fields"][$facetName];
        
        if($makeArray) {
            $array = $this->makeArrayWithSolrFacetField($array);
        }

        if($clean) {
            $this->cleanFacetFieldArray($array);
        }
      

        return $array;
    }
    /**
     * clean the result of getfacetfield. unset all value equal to 0
     * 
     * @param array
     * 
     */
    protected function cleanFacetFieldArray(&$array) {

            foreach($array as $key => $value) {
                if($value == 0) {
                    unset($array[$key]);
                }
            }
    }



    /**
     * @return float the average rank of the search. if numFound == 0, return false 
     * @param int the number of number you want after the dot for the rounded return 
     */
    public function getAverageOld($round = 2) {
        $arrayStars = $this->getFacetFieldStars();
        $sumRank = 0;
        $index = 0;
        // $rank = 0;


        foreach($arrayStars as $star => $value) {

            if($index%2 == 1) {
                $out = explode(".",$star);
                $star = ($out[0]+1);
                $sumRank += $star*$value;
                // $rank++;
            }
            
            $index++;
        }

        $total = $this->getNumFound();

        if($total <= 0) {
            return false;
        }

        $average = $sumRank/$total;

        return round($average, $round);
    }


    /**
     * @return float the average rank of the search. if numFound == 0, return false 
     * @param int the number of number you want after the dot for the rounded return 
     */
    public function getAverage($round = 2) {
        $arrayStars = $this->getFacetField(Solr::FACETFIELD_STARS);

       

        $totalStar = 0;
        $total = $this->getNumFound();

        foreach($arrayStars as $star => $value) {
            $out = explode(".",$star);
            $star = $out[0];
            $totalStar += $star * $value;
        }

        if($totalStar != 0) {
            $average = $totalStar/$total;

            if($this->mode == self::DEBUG) {
                $arrayDebug = array($arrayStars, $totalStar, $total, $average);
                $this->consoleSpeaker->speak($arrayDebug);
            }



            return round($average, $round);
        } else {
            return false;
        }


    }




    
    /**
     * print the array received in constructor 
     * 
     */
    public function printResponse() {
        echo "<pre>";
        print_r($this->response);
        echo "</pre>";
    }

    

    /**
     * Get the value of response
     */ 
    public function getResponse()
    {
        return $this->response;
    }
}