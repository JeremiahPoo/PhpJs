<?php 

class Debugator {


    public function __construct() {

    }

    public static function dump($var) {

        $line = ClassHelper::getOriginOffCall(ClassHelper::_LINE);
        $file = ClassHelper::getOriginOffCall(ClassHelper::_FILE);
        echo "<pre>";
        echo PHP_EOL;
        print_r($var);
        print_r(PHP_EOL."on line ".$line." in ".$file); 
        print_r(PHP_EOL."Type : [".gettype($var)."]".PHP_EOL);
        echo "</pre>";
        echo PHP_EOL;

    }
}