<?php 


require_once "Logator.php";
/*
Console Library Helper

Developped by Quentin Gary 

V 0.1 
*/
class ConsoleSpeaker {

    protected $logator;
    protected $path = __DIR__."/../log.text";
    protected $logActivated = false;

    public function __construct() {
        $this->logator = new Logator($this->path);
    }
    /** 
     * speak to the console
     * 
     * @param string or array $string the sentence to speak
     * 
     */
    public function speak($string) {

        if(is_string($string)) {
            echo $string.PHP_EOL;
            if($this->logActivated) {
                $this->logator->addLog($string);                   
            }
        } else if(is_array($string)) {
            echo "<pre>";
            print_r($string);
            echo "</pre>".PHP_EOL;
        }
        
    }
    
    /**
     * ask one thing to the console 
     * 
     * @param string $string the sentence to ask 
     * 
     * @return string $output the line entering by the user 
     */
    public function ask($string) {
        $this->speak($string);
        $output = readline(">>>");
        readline_add_history($output);

        return $output;
    }
    /** 
     * ask to the user a question with responses possibility 
     * typically a question is repeated while the user tape a correct result
     * Otherwise, the console repeat to ask the question
     * @param string $string the question to ask
     * @param array  $arrayOption the array wo contains the  options.
     *                            the key of the array are the proposition, the element is the speaked sentence when the user choose a proposition
     * @return int   the index of the selected response   
     */
    public function askWhile($string, $arrayOption) {

        $prePos = "<";
        $suPos = ">";
        $listPossibility = [];
        foreach($arrayOption as $key => $value) {
            $string .= $prePos.$key.$suPos;
        }
        
        while(1) {
            $output = $this->ask($string);
            $index = 0;
            foreach($arrayOption as $key => $value) {
                
                if($output == $key) {
                    $this->speak($value);
                    return $index;
                }
                $index ++;
            }
           
        }
    }

    public function logActive($bool) {
        if(!is_bool($bool)) {
            $type = gettype($bool);
            throw new Exception("the argument 0 of ".__FUNCTION__." must be a boolean. ".$type." received ");
        }
        $this->logActivated = $bool;
    }

    public function setPath($path) {
        $this->path = $path;
        $this->logator->setPath($path);

        return $this;
    }

    public function getPath() {
        return $this->logator->getPath();
    }

}