<?php 

require_once "ClassHelper.php";


class FilesManager {

    protected $path; 
    protected $file; 

    public function __construct($path = null) {

        if(!empty($path)) {
            $this->setPath($path);
        }
     


    }

    public function writeIn($string, $path = null) {
        if(!empty($path)) {
            $this->setPath($path);
        }

        if($this->path == null) {
            throw new Exception("The value of path must be setted. Use ".__CLASS__."::setPath for set this value" );
        }

        ClassHelper::TypeException($string, ClassHelper::_STR);
        $this->openFile();

        fwrite($this->file, $string);

        $this->closeFile();

    }

    public function extractJson($path = null) {
        if(!empty($path)) {
            $this->setPath($path);
        }

        if($this->path == null) {
            throw new Exception("The value of path must be setted. Use ".__CLASS__."::setPath for set this value" );
        }

        if(file_exists($this->path)) {
            $json = file_get_contents($this->path);
            $array = json_decode($json, true);

            if(!empty($array)) {
                return $array;
            } else {
                throw new Exception("Error when decoding the file ".$this->path );
            }
        } else {
            throw new Exception("The file ".$this->path." doesn't exists and / or you don't have the permissions");
        }


        return $array;
    }

    protected function openFile() {
        $this->file = fopen($this->path, "a");
    }

    protected function closeFile() {
        fclose($this->file);
    }


    /**
     * Get the value of path
     */ 
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set the value of path
     *
     * @return  self
     */ 
    public function setPath($path)
    {
        ClassHelper::TypeException($path,ClassHelper::_STR);
        $this->path = $path;

        return $this;
    }
}