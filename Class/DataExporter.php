<?php 

require_once "ConsoleSpeaker.php";


/**
 * 
 * 
 *  CLI Application for transfer sql table from a server to another server, compare database and changing the encoding 
 * 
 *  
 * 
 *  V 0.2 
 *  WIP
 *  Developped by Quentin Gary 
 */


class DataExporter {

    const START = "start";
    const DEST = "destination";
    const _LIST = "list";
    const ALL = "all"; 

    protected $database;
    protected $_PRINT;
    protected $psswd;
    protected $user;
    protected $host;
    protected $psswdDB;
    protected $userDB;
    protected $localServer;
    protected $destinationServer;
    protected $destinationUser;
    protected $destinationPasswd;
    protected $destinationUserDB;
    protected $destinationPsswdDB; 
    protected $destinationDatabase;

    protected $destinationRepository;
    protected $list;
    protected $listTableName = [];
    protected $destinationListTableName = [];

    protected $sqlDumpDirectory;

    protected $console;
    protected $logActive = false; 
    protected $logPath;

    protected $columnToEncode = [];


    public function __construct($config,$list, $logActive = false) {

        $args = func_get_args();

        $iArgs = 0;
        foreach($args as $arg) {
            if(!is_array($arg) && $iArgs != 2) {
                $type = gettype($arg);
                throw new Exception("The argument nummer ".$iArgs." must be a array.".$type." received" );
            } 

            if(empty($arg)) {
                throw new Exception("The arguments nummer ".$iArgs." is empty. Look README.MD and set your configurations files");
            }

            $iArgs++;
        }

    

        $this->console = new ConsoleSpeaker;

        $this->console->logActive($logActive);
        

        $this->localServer = $config["startServer"]["ip"];
        $this->host = $config["startServer"]["host"];
        $this->user = $config["startServer"]["user"];
        $this->psswd = $config["startServer"]["psswd"];

        $this->database = $config["startDatabase"]["name"];
        $this->userDB = $config["startDatabase"]["user"];
        $this->psswdDB = $config["startDatabase"]["psswd"];

        $this->destinationServer = $config["destinationServer"]["host"];
        $this->destinationUser = $config["destinationServer"]["user"];
        $this->destinationPasswd = $config["destinationServer"]["psswd"];
        $this->destinationRepository = $config["destinationServer"]["folder"];

        $this->destinationDatabase = $config["destinationDatabase"]["name"];
        $this->destinationUserDB = $config["destinationDatabase"]["user"];
        $this->destinationPsswdDB = $config["destinationDatabase"]["psswd"];

   

        $this->sqlDumpDirectory = __DIR__."/../dump";

        if(!file_exists($this->sqlDumpDirectory)) {
            mkdir($this->sqlDumpDirectory);
        }

        if(is_array($list)) {
            $this->list = $list; 
        } else {
            throw new Exception("The argument 1 must be a string or a array");
        }
       

        $this->makeTableList();

    }
    /**
     * dump all table in the list in the sqlDumpDirectory
     * and transfer them to the wished server and directory
     * 
     * @return self
     * 
     */
    public function makeTransfer() {
        
        $this->console->speak("Look if dump directory need a clean..."); 
        $this->cleanSqlDump();
        $this->console->speak("Starting transfer from ".$this->host." to ".$this->destinationServer."...");

        foreach($this->list as $table) {

            $this->checkTable($table);
            $this->sqlDump($table);
         
        }

        $this->importDataIntoDB();
        $this->cleanSqlDump();
        
        $this->console->speak("Transfer from ".$this->host." to ".$this->destinationServer." finished");

        return $this;
    } 
    /**
     * dump all table in the list in the sqlDumpDirectory
     * 
     * @return self 
     * 
     */
    public function makeSqlDump() {

        foreach($this->list as $table) {
            $this->checkTable($table);
            $this->sqlDump($table);
        }

        return $this;
    }
    /**
     * return a array with the table list and the number of row 
     * 
     */
    public function compareTable($mode = self::_LIST) {
        $startList = [];
        $destinationList = [];

        if($mode == self::_LIST) {
            $list = $this->list; 
        } elseif($mode == self::ALL) {
            $list = $this->listTableName;
        }
    


        $this->console->speak("Compare table...");

        foreach($this->destinationListTableName as $table) {
            $destinationList[$table] = $this->countTable($table, self::DEST);
        }

        foreach($list as $table) {
            $count = $this->countTable($table, self::START); 
            $encoding = $this->getEncoding($table);
            $inDb = $this->checkInDatabase($table);
            if($inDb) {
                $delta = $this->calcDelta($count , $destinationList[$table]); 
                $count_in_our_database = $destinationList[$table];
            } else {
                $delta = "NONE";
                $count_in_our_database = "NONE";
            }
   
            $startList[$table] = array( "count" => $count,
                                        "encoding" => $encoding,
                                        "in_database" => $inDb,
                                        "count_in_our_database" => $count_in_our_database,
                                        "delta" => $delta 
                                        );
        }
        return $startList; 
    }

    /**
     * 
     * Calcul the difference between the number of column of the two database  
     * 
     * @return int delta  
     */
    protected function calcDelta($startCount, $destCount) {
        $this->console->speak("Calc delta...");
        return $destCount - $startCount; 
    }
    /**
     * 
     * check if the database is already present in the destination db 
     * 
     * @return boolean 
     * 
     */
    protected function checkInDatabase($table) {
        $this->console->speak("Check if the table ".$table." is already in the destination database...");
        if(in_array($table , $this->destinationListTableName)) {
            return true;
        } else {
            return false; 
        }
    }

    protected function getEncoding($table) {

        $this->console->speak("Get encoding of ".$table."...");

        $host = $this->host;
        $database = $this->database;
        $userDB = $this->userDB;
        $psswdDB = $this->psswdDB; 

        $db = new PDO('mysql:host='.$host.';dbname='.$database, $userDB, $psswdDB);
        $query = "SHOW TABLE STATUS LIKE '".htmlspecialchars($table)."'";
        $stmt = $db->query($query);
        if($stmt->execute()) {
            $encoding = $stmt->fetch()["Collation"];
        }

        return $encoding;
    }
    /**
     * 
     * return the number of row of a table 
     */
    protected function countTable($table, $server) {

        if($server == self::START) {
            $host = $this->host;
            $database = $this->database;
            $userDB = $this->userDB;
            $psswdDB = $this->psswdDB; 
           
        } elseif($server == self::DEST) {
            $host = $this->destinationServer;
            $database = $this->destinationDatabase;
            $userDB = $this->destinationUserDB;
            $psswdDB = $this->destinationPsswdDB; 
         
        }
        $db = new PDO('mysql:host='.$host.';dbname='.$database, $userDB, $psswdDB);
        $query = "SELECT count(*) FROM ".htmlspecialchars($table); 
        $stmt = $db->query($query);
        if($stmt->execute()) {
            $numberOfRow = $stmt->fetch()[0];
        }

        return $numberOfRow;
    }
    /**
     * transfer all sql files in the sqlDumpDirectory to the destinationRepository of the destinationServer 
     * 
     * @return self
     */
    public function makeScpTransfer() {

        foreach($this->list as $table) {
            $this->scpTransfer($table);
        }
    }

    protected function sqlDump($table) {
        
        $file = $this->getTempFilesLogin();

        $stringToExec = "mysqldump --defaults-file=".$file;
        $stringToExec .= " -h ".$this->host; 
        $stringToExec .= " --databases ".$this->database;
        $stringToExec .= " --tables ".$table." > ".$this->sqlDumpDirectory.DIRECTORY_SEPARATOR.$table.".sql";

        if($this->_PRINT) {
            $this->console->speak($stringToExec);
        } else {
            $this->console->speak("Executing : ".$stringToExec);
            exec($stringToExec);
        }

        unlink($file);
    }

    protected function setColumnToEncode($column) {
        ClassHelper::TypeException($column,ClassHelper::_ARRAY);
        $this->columnToEncode = $column;
    }

    /**
     * 
     * convert table to the wished encoding 
     * 
     * @param string table : the table you want to convert 
     * @param string encoding : the encoding you want
     * 
     * 
     * @return bool : true in case of success, false in case of defeat 
     */
    protected function convertEncoding($table, $encoding = "utf8_general_ci") {
        $host = $this->host;
        $database = $this->database;
        $userDB = $this->userDB;
        $psswdDB = $this->psswdDB; 

        $db = new PDO('mysql:host='.$host.';dbname='.$database, $userDB, $psswdDB);
        $columns = $this->columnToEncode; 

        foreach($columns as $column) {
            $query = "ALTER TABLE `".htmlspecialchars($table)."` CHANGE `".htmlspecialchars($column)."` `".htmlspecialchars($column)."` TEXT CHARACTER SET utf8 COLLATE ".htmlspecialchars($encoding)." NULL DEFAULT NULL"; 
            $stmt = $db->query($query);
    
            if($stmt->execute()) {
                $this->console->speak("Encoding change of the column ".$column." of the table ".$table." : success");
            } else {
                $this->console->speak("ERROR when changing the encoding of the column ".$column." of table ".$table);
                $this->console->speak($query);
            }
        }

      

        $query = "ALTER TABLE `".htmlspecialchars($table)."` DEFAULT CHARACTER SET utf8 COLLATE ".htmlspecialchars($encoding); 
        $stmt = $db->query($query);

        if($stmt->execute()) {
            $this->console->speak("Encoding change of the table ".$table." success");

            return true;

        } else {
            $this->console->speak("ERROR when changing the encoding of table ".$table);
            $this->console->speak($query);

            return false; 
        }
 
    }
    /**
     * 
     * import the dumped sql files to the mysql of the destination server 
     * 
     */
    protected function importDataIntoDB() {

        $user = $this->destinationUserDB; 
        $password = $this->destinationPsswdDB;
        $databasename = $this->destinationDatabase;
        $destinationHost = $this->destinationServer;

        if(empty($user)) {
            throw new Exception("You must configure your destination DB user (Look the config.json file)");
        }

        if(empty($password)) {
            throw new Exception("You must configure your destination DB password (Look the config.json file)");
        }

        if(empty($databasename)) {
             throw new Exception("You must configure your destination DB name (Look the config.json file)");
        }


        //create a temporary file
        $file   = tempnam(sys_get_temp_dir(), 'mysql');

        //store the configuration options
        file_put_contents($file, 
        "[client]
        user={$user}
        password=\"{$password}\"");

        foreach($this->list as $list) {

            $filename = $list.".sql"; 

            $this->checkDestinationTable($list);
        
            $string = "mysql --defaults-file=".$file." -h ".$destinationHost." ".$databasename." < ".$this->sqlDumpDirectory. DIRECTORY_SEPARATOR .$filename;

         
            $this->console->speak("Executing : ".$string);
            passthru($string);
            $this->console->speak("Importation of ".$list." finished");
        
        }

        unlink($file);

    }

    

    /**               
     * 
     * make a scp transfer from the localsercer to the destination server with the config credency's
     * 
     * 
     */
    public function scpTransfer($file, $mime = ".sql") {


        $hostname = $this->destinationServer; 
        $username = $this->destinationUser; 
        $password = $this->destinationPasswd; 
        if($mime != ".sql") {
            $sourceFile = $file.$mime; 
        } else {
            $sourceFile = $this->sqlDumpDirectory. DIRECTORY_SEPARATOR .$file.$mime; 
        }
        $targetFile = $this->destinationRepository. DIRECTORY_SEPARATOR .$file.$mime;
        $this->console->speak("sourceFile : ".$sourceFile);
        $this->console->speak("targetFile : ".$targetFile);
        $connection = ssh2_connect($hostname); 
        ssh2_auth_password($connection, $username, $password);
        ssh2_scp_send($connection, $sourceFile, $targetFile, 0644);

        $this->console->speak("Make scp transfer from ".$this->host." to ".$hostname.". File => ".$sourceFile);


    }

    /**
     * check if table exist in the start database 
     * 
     */
    protected function checkTable($table) {
        if(!in_array($table, $this->listTableName)) {
            throw new Exception("The table ".$table." is not in your database : ".$this->database);
        }
    }
    /**
     * 
     * check if table already exist in the destination database 
     */
    protected function checkDestinationTable($table) {
        if(in_array($table, $this->destinationListTableName)) {
            throw new Exception("The table ".$table." is already in your destination database : ".$this->destinationDatabase);
        }
    }
    /**
     * make a table list of the table wo are present on the database
     * 
     */
    protected function makeTableList() {

        for($i = 0 ; $i < 2 ; $i++) {
            if($i == 0) {
                $host = $this->host;
                $database = $this->database;
                $userDB = $this->userDB;
                $psswdDB = $this->psswdDB; 
               
            } elseif($i == 1) {
                $host = $this->destinationServer;
                $database = $this->destinationDatabase;
                $userDB = $this->destinationUserDB;
                $psswdDB = $this->destinationPsswdDB; 
             
            }

            $listTableName = [];
            $this->console->speak("Make table list of ".$host." dbname=".$database." Credency's : u :".$userDB." p : ".$psswdDB);
            $db = new PDO('mysql:host='.$host.';dbname='.$database, $userDB, $psswdDB);
            $query = "SHOW TABLES FROM ".$database; 
            $stmt = $db->query($query);
            if($stmt->execute()) {
    
                $tableList = $stmt->fetchAll(); //["Tables_in_".$this->database]
    
                foreach($tableList as $table) {
                    array_push($listTableName, $table[0]);
                }
                 
            }
    
            if($i == 0) {
                $this->listTableName = $listTableName;
            } elseif($i == 1) {
                $this->destinationListTableName = $listTableName;
            }

        }

       
    }

    protected function makeList($list) {
        if(!is_string($list)) {
            throw new Exception("the list value must be a string like 'table1,table2,table3'");
        }
        $arrayList = explode(",",$list);

        return $arrayList;
    }
    /**
     * create temporary files for stock credency and use it for the sql dump 
     * 
     * @return string the filesname 
     */
    protected function getTempFilesLogin() {
        //create a temporary file
        $file   = tempnam(sys_get_temp_dir(), 'mysqldump');

        //store the configuration options
        file_put_contents($file, 
        "[mysqldump]
        user={$this->userDB}
        password=\"{$this->psswdDB}\"");


        return $file;
    }
    
    protected function printWord($string) {
        $this->console->speak($string);
    }

    protected function cleanSqlDump() {
        
        $files = glob($this->sqlDumpDirectory.'/*'); // get all file names

        if(empty($files)) {
            return null; 
        }

        $this->console->speak('start clean sql dump folder..');


        foreach($files as $file){ // iterate file

            if(is_file($file)) {
                unlink($file); // delete file
                $this->console->speak('File '.$file.' deleted');
            }   

        }

        $this->console->speak('end clean sql dump folder.');
    }
    /**
     * print on the console the table list of the database 
     */
    public function printTableList() {
        $this->console->speak($this->getListTableName());
    }

    /**
     * Get the value of _PRINT
     */ 
    public function get_PRINT()
    {
        return $this->_PRINT;
    }

    /**
     * Set the value of _PRINT
     *
     * @return  self
     */ 
    public function set_PRINT($_PRINT)
    {
        if(!is_bool($_PRINT)) {
            throw new Exception("Value must be a boolean");
        }
        $this->_PRINT = $_PRINT;

        return $this;
    }

    /**
     * Get the value of listTableName
     */ 
    public function getListTableName()
    {
        return $this->listTableName;
    }

    /**
     * Set the value of listTableName
     *
     * @return  self
     */ 
    public function setListTableName($listTableName)
    {
        $this->listTableName = $listTableName;

        return $this;
    }

    /**
     * Get the value of logPath
     */ 
    public function getLogPath()
    {
        return $this->console->getPath();
    }

    /**
     * Set the value of logPath
     *
     * @return  self
     */ 
    public function setLogPath($logPath)
    {

        $this->console->setPath($logPath);

        return $this;
    }

    /**
     * Get the value of logActive
     */ 
    public function getLogActive()
    {
        return $this->console->getLogActive();
    }

    /**
     * Set the value of logActive
     *
     * @return  self
     */ 
    public function setLogActive($logActive)
    {
        $this->console->logActive($logActive);

        return $this;
    }



}