<?php
/**
 *  Log class 
 * 
 *  Developped by Quentin Gary 
 * 
 *  V 0.1 
 */
class Logator {

    protected $path;

    public function __construct($path) {
        $this->path = $path; 


    }

    public function addLog($msg) {
        $path = $this->path; 
        $file = fopen($path, 'a'); 

        $script_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
        $str = date('[d/M/Y H-i-s]')." In ".$script_name.".php : ".$msg.PHP_EOL;
  
        fwrite($file , $str);
        fclose($file);
    } 
    
    public function setPath($path) {
        $this->path = $path;

    }

    public function getPath() {
        return $this->path;
    }


}