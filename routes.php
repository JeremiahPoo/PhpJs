<?php
/**
 * Register your routes here in the form :
 * 
 *  $app->router->addRoute("pageName", "classname::methodname");
 * 
 */
$app->router->addRoute("main/[{arg}]/[{arg2}]", "MainController::main");
$app->router->addRoute("secondPage", "MainController::secondPage");
$app->router->addRoute("table", "MainController::tablePage");
$app->router->addRoute("anotherPage", "MainController::anotherPage");