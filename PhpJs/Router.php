<?php 
/**
 * Router Class 
 * 
 * @author Quentin Gary 
 */
class Router {
    protected $server;
    protected $routes;
    public $container;
    

    public function __construct() {
        $this->setServer($_SERVER);
        $this->container =  new Container;
    }
    /**
     * set this server 
     * @param array $server the $_SERVER 
     */
    public function setServer($server) {
        $this->server = (object) $server; 
    }
    /**
     * @return $server 
     * 
     */
    public function getServer() {
        return $this->server;
    }
    /**
     * give the path info 
     * 
     * @return string path info 
     * 
     */
    public function getPathInfo() {
        return $this->server->PATH_INFO;
    }
    /**
     * 
     * add a route to the router 
     * 
     * @param string $path : the path fo access route 
     * @param string $action : the function called when path is enterign by the user 
     */
    public function addRoute($path, $action) {
        $route = new Route($path, $action);
        $this->routes[$route->getKey()] = $route;
    }
    /**
     * look what path have entered the user 
     * and call the right function 
     * 
     */
    public function matchRoute() {
        $out = explode("/",$this->getPathInfo());
    
        
        $function =  $this->routes[$out[1]]->getAction();
        $arguments = [];
        for($i = 2 ; $i < count($out) ; $i++) {
            $arguments[] = $out[$i];
        }

        $this->routes[$out[1]]->setArguments($arguments);
        $out = explode("::",$function);
     
        call_user_func( array($this->container->getController($out[0]),$out[1])); 
    }
    /**
     * 
     * get arguments of the current route 
     * 
     * @return array all the arguments of the route 
     * 
     */
    public function getArguments() {
        $path = explode("/", $this->getPathInfo());
     
        return $this->routes[$path[1]]->getArguments();
    }
    /**
     * 
     * get arguments with the key given in param of the current route 
     * 
     * @param string $key : the key of the argument 
     * @return mixed all the arguments of the route 
     * 
     */
    public function getArgument($key) {
        $path = explode("/", $this->getPathInfo());
     
        return $this->routes[$path[1]]->getArgument($key);
    }
    /**
     * @return array this routes 
     * 
     */
    public function getRoutes() {
        return $this->routes;
    }

}