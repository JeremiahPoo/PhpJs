<?php 
/**
 * Container Class 
 * 
 * @author Quentin Gary 
 */
class Container {

    protected $Controllers;
    
    public function __construct() {

    }
    /**
     * add a controller to the container 
     * 
     * @param string $controllerName : the classname of the controller 
     * @param App $app : the application 
     */
    public function addController($controllerName, $app) {
        $this->Controllers[$controllerName] = new $controllerName($app);
    }
    /**
     * @param string $controllerName : the classname of the controller you want to access
     * @return Controller 
     */
    public function getController($controllerName) {
        return $this->Controllers[$controllerName];
    }
}