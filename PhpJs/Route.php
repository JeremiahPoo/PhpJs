<?php 

/**
 * Route Class 
 * 
 * @author Quentin Gary 
 */
class Route {

    protected $action;
    protected $key;
    protected $arguments = []; 

    public function __construct($path, $action) {
        $this->explodePath($path);
        $this->action = $action;
    }
    /**
     * explode the path and put the name of the route in $key and the arguments in $arguments
     * 
     * @param string $path : the url 
     * 
     */
    protected function explodePath($path) {
        $out = explode("/",$path);
      
        $key = $out[0];
        $arguments = []; 

        for($i = 1 ; $i < count($out) ; $i++) {
            $arguments[$out[$i]] ="";
        }

        $this->key = $key; 
        $this->arguments[] = is_array($arguments) ? $arguments : []; 
   

 
    }
    /**
     * 
     * @return array $arguments 
     */
    public function getArguments(){
        return $this->arguments[0]; 
    }
    /**
     * @param string $key 
     * @return string the argument who have the key 
     * or null if the argument doesn't exist 
     */
    public function getArgument($key){
        

        foreach( $this->arguments[0] as $keyArg => $arg) {
            $replace =  str_replace(array("{","}","[","]"), "", $keyArg) ;

            if($key == $replace) {
                return $arg;
            }
        }
        return null; 
    }
    /**
     * set arguments value 
     * 
     * @param mixed the value you want to put in argument 
     */
    public function setArguments($value){
      
        $index = 0;
  
        foreach($this->arguments[0] as &$arg) {
            if(!isset($value[$index])) {
                continue; 
            }
            $arg = $value[$index];
            
            $index++;
        }
        
    }

    /**
     * @return strig $action : the action of the route 
     * 
     */
    public function getAction(){
        return $this->action; 
    }
    /**
     * @return strig $action : the key of the route 
     * 
     */
    public function getKey(){
        return $this->key; 
    }
}