<?php 
/**
 * App Class 
 * 
 * @author Quentin Gary 
 */
class App {
    
    public $router;
    protected $basePath; 
    protected $index = "index.php";
    protected $jsonPath = "TemporaryFiles/routes.json";
    protected $fetchActivated = true; 

    public function __construct() {
        $this->router = new Router; 
        $dirPathArray = explode('/', $_SERVER['SCRIPT_NAME']);

        // remove path's last part
        array_pop($dirPathArray);
        $dir = implode('/', $dirPathArray);
        $this->basePath = $dir;
  
    }
    /**
     * run the app 
     * 
     */
    public function run() {
        $this->router->matchRoute();
        $this->createJsonPathFile();
    }
    /**
     * 
     * 
     * @return Container the container of the router 
     */
    public function getContainer() {
        return $this->router->container; 
    }
    /**
     * @param string $routeName : the name of the route you want to have the path 
     * @return string the path of the route 
     */
    public function getPath($routeName) {
    
        if(isset($this->router->getRoutes()[$routeName])) {

            return $this->basePath."/".$this->index."/".$routeName;
        } else {
            throw new Exception("The specified route doesn't exist.");
        }
    }
    /**
     * 
     * @return string basePath : the basePath for construct link to asset or link to route 
     */
    public function getBasePath() {
        return $this->basePath;
    }
    /**
     * shortcut for add a controller to the Container
     * @param string $controllerName : the classname of the controller 
     */
    public function addController($controllerName) {
        $this->getContainer()->addController($controllerName, $this);
    }
    /**
     * set index 
     * @param string index  
     */
    public function setIndex($index) {
        $this->index = $index;
    }
    /**
     * @return string index 
     */
    public function getIndex() {
        return $this->index; 
    }
    /**
     * 
     * create json file. This one contain a key / value array with 
     * the name of the route in key and the path for acces herr in value 
     * 
     */
    protected function createJsonPathFile() {

        $routesArray = []; 
        foreach($this->router->getRoutes() as $key => $value ) {
            $routesArray[$key] = $this->getPath($key);
        }
        $json = json_encode($routesArray, JSON_PRETTY_PRINT);


        $file = fopen($this->jsonPath, "w"); 
        fwrite($file, $json);
        fclose($file);
        

    }
    
    

    /**
     * Get the value of fetchActivated
     */ 
    public function getFetchActivated()
    {
        return $this->fetchActivated;
    }

    /**
     * Set the value of fetchActivated
     *
     * @return  self
     */ 
    public function setFetchActivated($fetchActivated)
    {
        $this->fetchActivated = $fetchActivated;

        return $this;
    }
}