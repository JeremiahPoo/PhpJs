<?php 
/**
 * Render Class 
 * 
 * @author Quentin Gary 
 */
class Render {

    protected $jsonPath = "/TemporaryFiles/routes.json";

    protected $fetchActivated = true;
    /**
     * render a templating 
     * 
     * @param string $path : the path of the template 
     * @param array $args : the variables you want to pass to the template. Convert the array to var with extract 
     * 
     */
    public function view($path, $args = array()) {

        extract($args, EXTR_PREFIX_SAME, "wddx");
        
        include $path;
        $this->putFetchPhpJsInDom();
    }
    /**
     * 
     * render a Component 
     * @param string $path : the path of the template 
     * @param array $args : the variables you want to pass to the template. Convert the array to var with extract 
     * 
     */
    public function fetchView($path, $args = array()) {
        extract($args, EXTR_PREFIX_SAME, "wddx");
        include $path;
    }

    public function putFetchPhpJsInDom() {
        // Améliorable mais fait le taff pour le moment
        $out = explode("/",$_SERVER["REQUEST_URI"]);
        array_pop($out); // remove main
        array_pop($out); // remove index.php
        $dir = implode('/', $out);
        $jsonPath = $dir . $this->jsonPath;

        $content = file_get_contents(__DIR__."/../js/FetchPhpJs.js.dist.min");
        $content = str_replace("{{[path]}}", "\"".$jsonPath."\"", $content);
        echo "<script>".$content."</script>"; 
    } // activte fetch



    /**
     * Get the value of fetchActivated
     */ 
    public function getFetchActivated()
    {
        return $this->fetchActivated;
    }

    /**
     * Set the value of fetchActivated
     *
     * @return  self
     */ 
    public function setFetchActivated($fetchActivated)
    {
        $this->fetchActivated = $fetchActivated;

        return $this;
    }
}