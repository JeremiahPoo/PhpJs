<?php 
/**
 * Controller Class 
 * 
 * The parent class of all Controller 
 * 
 * @author Quentin Gary 
 */
abstract class Controller {

    protected $render;
    protected $app; 

    final public function __construct($app) {
        $this->render = new Render; 
        $this->render->setFetchActivated($app->getFetchActivated());
        $this->app = $app;
    }
    /**
     * shortcut fot acces router arguments
     * @param string the key of the argumetn 
     * @return mixed the value of the argument 
     */
    public function getArgument($key) {
        return $this->app->router->getArgument($key);
    }

}