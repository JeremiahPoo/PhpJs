<?php 



error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);


spl_autoload_register('myAutoloader');


/**
 * 
 * 
 *  To Do : 
 *  
 * Refactoring the autoloader 
 * => array with path and foreach 
 * 
 */ 

function myAutoloader($className) {
    $path = __DIR__."/Class/";
    $fileName = $path.$className.'.php';

    if(file_exists($fileName)) {
        include $fileName;
    } else {
        $path = __DIR__."/Controllers/";
        $fileName = $path.$className.'.php';
    
        if(file_exists($fileName)) {
            include $fileName;
        } else {
            $path = __DIR__."/PhpJs/";
            $fileName = $path.$className.'.php';
        
            if(file_exists($fileName)) {
                include $fileName;
            } 
        }
    }
}




$app = new App;

require_once __DIR__."/dependency.php";

require_once __DIR__."/routes.php";

$app->run();


